from flask import Flask, request, jsonify, request_started, g
from typing import Any
import structlog
import logging
import os
import sys
import uuid

from .exceptions import APIError, APIMalformedHeaders

logging.basicConfig(
    format="%(message)s",
    stream=sys.stdout,
    level=getattr(logging, os.environ.get("IP_LOG_LEVEL", "info").upper()),
)
structlog.configure(
    wrapper_class=structlog.make_filtering_bound_logger(
        getattr(logging, os.environ.get("IP_LOG_LEVEL", "info").upper())
    ),
    processors=[
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.ExceptionRenderer(),
        structlog.processors.StackInfoRenderer(),
        structlog.contextvars.merge_contextvars,
        structlog.dev.ConsoleRenderer(),
    ],
    logger_factory=structlog.stdlib.LoggerFactory(),
)

log = structlog.stdlib.get_logger()
app = Flask(__name__)


@request_started.connect_via(app)
def bind_request_details(sender: Flask, **extras: dict[str, Any]) -> None:
    request_id: str = str(uuid.uuid4())
    g.request_id: str = request_id
    structlog.contextvars.clear_contextvars()
    structlog.contextvars.bind_contextvars(
        request_id=request_id,
        view=request.path,
        peer=request.access_route[0],
    )


@app.after_request
def append_unique_id(response):
    response_id: str = str(uuid.uuid4())
    response.headers.add("X-Unique-ID", response_id)
    response.headers.add("X-Response-ID", response_id)
    response.headers.add("X-Request-ID", g.request_id)
    # structlog.contextvars.clear_contextvars()
    structlog.contextvars.bind_contextvars(
        response_id=response_id,
        request_id=g.request_id,
        status_code=response.status_code,
        status=response.status,
        content_type=response.content_type,
        headers={**response.headers},
    )
    log.debug("response")
    return response


@app.before_request
def fix_transfer_encoding():
    """
    Sets the "wsgi.input_terminated" environment flag, thus enabling
    Werkzeug to pass chunked requests as streams.  The gunicorn server
    should set this, but it's not yet been implemented.
    """

    transfer_encoding = request.headers.get("Transfer-Encoding", None)
    if transfer_encoding == "chunked":
        request.environ["wsgi.input_terminated"] = True


@app.errorhandler(APIError)
def handle_exception(error):
    response = error.to_dict()
    # response.status_code = error.status_code
    log.error(
        f"{error.description}: {error.message}",
        message=error.message,
        description=error.description,
        code=error.code,
        response=response,
        exc_info=error,
    )
    return jsonify(response), error.code
