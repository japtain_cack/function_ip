from flask import Response
from werkzeug.exceptions import InternalServerError
from requests import get
from ipaddress import ip_address, ip_network
import re
import structlog
import os

from function import app
from .exceptions import APIError, APIMalformedHeaders

log = structlog.stdlib.get_logger()
local_net = ip_network(os.environ.get("IP_LOCAL_NET", "10.0.0.0/8"))


def is_ipaddress(ip) -> bool:
    try:
        return ip_address(ip.strip())
    except:
        return False


def is_local_address(ip) -> bool:
    try:
        return ip_address(ip) in local_net
    except:
        return False


def lookup_ip() -> str:
    r: str = get("https://api.ipify.org")
    return str(r.content.decode("utf8").strip())


def get_ip(headers: dict, args: dict = {}) -> str:
    if not headers:
        raise APIMalformedHeaders(
            "Headers is None...",
            payload={**headers},
        )

    ip: str = None
    cf_ip = headers.get("Cf-Connecting-Ip")
    real_ip = headers.get("X-Real-Ip")
    client_ip = headers.get("X-Client-Ip")
    forwarded_for_ip = [
        addr.strip() for addr in headers.get("X-Forwarded-For", "").split(",")
    ][0]

    addresses = [
        cf_ip,
        real_ip,
        client_ip,
        forwarded_for_ip,
    ]
    log.debug(
        "addresses collected [cf_ip, real_ip, client_ip, forwarded_for_ip]",
        addresses=addresses,
    )

    if args.get("resolve-any", "true").lower() == "true":
        ip = cf_ip or real_ip or client_ip or forwarded_for_ip
        log.debug(
            "resolve-any enabled, will try to get any available IP. caution, may not be reliable",
            address=ip,
        )

    for addr in addresses:
        if addr and not is_local_address(addr):
            ip = addr
            log.debug(
                "found non-local address",
                address=addr,
            )

    if not ip or not is_ipaddress(ip):
        raise APIMalformedHeaders(
            "IP could not be determined from headers. Ip is malformed or None.",
            payload=headers,
        )

    if (
        ip_address(ip) in local_net
        and args.get("resolve-local", "true").lower() == "true"
    ):
        ip = lookup_ip()
        log.debug(
            " resolve-local is true and an internal ip was detected, resoliving with ipify service.",
            address=ip,
        )

    log.debug("real ip obtained", real_ip=ip, headers={**headers}, args=args)
    return ip.strip()


def handle(request) -> Response:
    try:
        headers: dict = getattr(request, "headers", {})
        args: dict = getattr(request, "args", {})
        ip: str = get_ip(headers, args)

        structlog.contextvars.bind_contextvars(
            headers={**headers},
            args=args,
            real_ip=ip,
        )

        message: str = str(ip)
        if args.get("headers"):
            message = f"{ip}\n\n{str(headers)}"

        return Response(f"{message}", mimetype="text/plain"), 200

    except APIMalformedHeaders as error:
        raise error
    except Exception as error:
        raise APIError("Unhandled exception", code=500, payload={}) from error
