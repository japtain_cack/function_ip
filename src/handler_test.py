from ipaddress import ip_address, ip_network
import logging
import pytest
import re
import os
import sys

from flask import Response
from werkzeug.exceptions import InternalServerError, BadRequest
import structlog

from .exceptions import APIMalformedHeaders
from .handler import handle

logging.basicConfig(
    format="%(message)s",
    stream=sys.stdout,
    level=getattr(logging, os.environ.get("IP_LOG_LEVEL", "info").upper()),
)
structlog.configure(
    wrapper_class=structlog.make_filtering_bound_logger(
        getattr(logging, os.environ.get("IP_LOG_LEVEL", "info").upper())
    ),
    processors=[
        structlog.stdlib.add_logger_name,
        structlog.stdlib.add_log_level,
        structlog.stdlib.PositionalArgumentsFormatter(),
        structlog.processors.ExceptionRenderer(),
        structlog.processors.StackInfoRenderer(),
        structlog.contextvars.merge_contextvars,
        structlog.dev.ConsoleRenderer(),
    ],
    logger_factory=structlog.stdlib.LoggerFactory(),
)

log = structlog.stdlib.get_logger()

# To disable testing, you can set the build_arg `TEST_ENABLED=false` on the CLI or in your stack.yml
# https://docs.openfaas.com/reference/yaml/#function-build-args-build-args


def is_ipaddress(ip):
    try:
        return ip_address(ip.strip())
    except:
        return False


def is_local_address(ip) -> bool:
    try:
        return ip_address(ip) in ip_network(
            os.environ.get("IP_LOCAL_NET", "10.0.0.0/8")
        )
    except:
        return False


def is_gateway_address(ip) -> bool:
    try:
        return ip_address(ip) in ip_network(
            os.environ.get("IP_GATEWAY_NET", "63.227.129.130/32")
        )
    except:
        return False


def test_client_ip_header():
    """
    Test to verify that the x-client-ip header is
    handled properly.
    """
    r = Response()
    r.headers["X-Client-Ip"] = "1.1.1.1"
    r.args = {"foo": "bar"}

    resp, status = handle(r)

    assert is_ipaddress(str(resp.data.decode("utf-8")))
    assert resp.status_code, 200


def test_real_ip_header():
    """
    Test to verify that the x-real-ip header is
    handled properly.
    """
    r = Response()
    r.headers["X-Real-Ip"] = "1.1.1.1"
    r.args = {"foo": "bar"}

    resp, status = handle(r)

    assert is_ipaddress(str(resp.data.decode("utf-8")))
    assert resp.status_code, 200


def test_cloudflare_header():
    """
    Test to verify that the cf-connecting-ip header is
    handled properly.
    """
    r = Response()
    r.headers["Cf-Connecting-Ip"] = "1.1.1.1"
    r.args = {"foo": "bar"}

    resp, status = handle(r)

    assert is_ipaddress(str(resp.data.decode("utf-8")))
    assert resp.status_code, 200


def test_forwarded_for_header():
    """
    Test to verify that multiple IPs in x-forwarded-for
    are handled properly. Only the far left IP should be
    returned. Which should be your public IP.
    """
    r = Response()
    r.headers["X-Forwarded-For"] = "1.1.1.1, 1.0.0.1"
    r.args = {"foo": "bar"}

    resp, status = handle(r)

    assert is_ipaddress(str(resp.data.decode("utf-8")))
    assert resp.status_code, 200


def test_no_ip():
    """
    Ensure the proper exception is thrown when no IP
    could be determined.
    """
    r = Response()
    r.headers["X-Client-Ip"] = ""

    with pytest.raises(APIMalformedHeaders) as exc_info:
        resp, status = handle(r)
    assert exc_info.value.code, 400


def test_returns_local_address():
    """
    Test arguments reesolver-local=false on local addresses.
    This test should return a local address because it disables
    the ipify service for looking up the public IP on local nets.
    """
    r = Response()
    r.headers["X-Real-Ip"] = "10.100.1.1"
    r.args = {"resolve-local": "False", "resolve-any": "true"}

    resp, status = handle(r)

    assert is_ipaddress(str(resp.data.decode("utf-8"))), True
    assert resp.status_code, 200


def test_no_returns_local_address():
    """
    Ensure that a public IP is returned for local addresses.
    Similar to test_returns_gateway_address, but is not specific
    to the gateway address.
    """
    r = Response()
    r.headers["X-Real-Ip"] = "10.100.1.1"

    resp, status = handle(r)

    assert is_local_address(str(resp.data.decode("utf-8"))) == False
    assert resp.status_code, 200


def test_returns_gateway_address():
    """
    Ensure the gateway IP is returned for local addresses.
    """
    r = Response()
    r.headers["X-Real-Ip"] = "10.100.1.1"

    resp, status = handle(r)

    assert is_gateway_address(str(resp.data.decode("utf-8"))), True
    assert resp.status_code, 200


def test_no_returns_gateway_address():
    """
    Ensure the gateway IP isn't returned for pulbic IP addresses.
    """
    r = Response()
    r.headers["X-Real-Ip"] = "1.1.1.1"
    r.args = {"resolve-local": "False", "resolve-any": "false"}

    resp, status = handle(r)

    assert is_gateway_address(str(resp.data.decode("utf-8"))) == False


def test_args_headers_true():
    """
    Test args headers=true
    """
    r = Response()
    r.headers["X-Real-Ip"] = "1.1.1.1"
    r.args = {"headers": "true"}

    resp, status = handle(r)

    assert re.search(
        r".*x-real-ip.*",
        str(resp.data.decode("utf-8")),
        re.I | re.M,
    )
    assert re.search(
        r"x-real-ip:\s?((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$|\W)){4}",
        str(resp.data.decode("utf-8")),
        re.I | re.M,
    )
    assert resp.status_code, 200
