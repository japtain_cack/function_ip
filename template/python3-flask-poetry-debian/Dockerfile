######## Stage: watchdog ########
FROM ghcr.io/openfaas/of-watchdog AS watchdog

######## Stage: build ########
FROM python:3.12-slim-bullseye AS build
  USER root
  ARG ADDITIONAL_PACKAGES
  ENV DEBIAN_FRONTEND=noninteractive
  ENV LANG=en_US.UTF-8
  ENV LANGUAGE=en_US:en
  ENV LC_ALL=en_US.UTF-8
  ENV VIRTUAL_ENV="./venv"
  ENV POETRY_VIRTUALENVS_PATH="$VIRTUAL_ENV"
  ENV POETRY_VIRTUALENVS_IN_PROJECT="true"
  ENV POETRY_VIRTUALENVS_OPTIONS_ALWAYS_COPY="true"

  # Update and install system packages
  RUN apt-get -qy update \
      && apt-get -qy install gcc make locales ${ADDITIONAL_PACKAGES} \
      && rm -rf /var/lib/apt/lists/*

  # Build the function directory and install any user-specified components
  RUN mkdir -p function \
      && touch ./function/__init__.py

  COPY function/pyproject.toml function/

  RUN python -m venv $VIRTUAL_ENV --copies && \
      $VIRTUAL_ENV/bin/pip install --no-cache-dir -U pip setuptools && \
      $VIRTUAL_ENV/bin/pip install --no-cache-dir -U poetry && \
      $VIRTUAL_ENV/bin/poetry config --list

  RUN $VIRTUAL_ENV/bin/poetry -C function/ install --only main && \
      $VIRTUAL_ENV/bin/poetry -C function/ show
  
  RUN touch ./depends_on

######## Stage: test ########
FROM python:3.12-slim-bullseye AS test
  USER root
  ENV DEBIAN_FRONTEND=noninteractive
  ENV LANG=en_US.UTF-8
  ENV LANGUAGE=en_US:en
  ENV LC_ALL=en_US.UTF-8
  ENV VIRTUAL_ENV="venv"
  ENV POETRY_VIRTUALENVS_PATH="$VIRTUAL_ENV"
  ENV POETRY_VIRTUALENVS_IN_PROJECT="true"
  ENV POETRY_VIRTUALENVS_OPTIONS_ALWAYS_COPY="true"
  ARG TEST_COMMAND="$VIRTUAL_ENV/bin/tox -c function/"
  ARG TEST_ENABLED=true

  COPY function/ function/
  COPY index.py .
  COPY --from=build venv $VIRTUAL_ENV

  RUN $VIRTUAL_ENV/bin/python -m poetry export -C function/ --without-hashes --with dev --format requirements.txt --output function/requirements.txt && \
      $VIRTUAL_ENV/bin/python -m poetry install -C function/ --with tox

  RUN [ "$TEST_ENABLED" = "false" ] && echo "skipping tests" || eval "$TEST_COMMAND"

  COPY --from=build depends_on .

######## Stage: ship ########
FROM python:3.12-slim-bullseye AS ship
  USER root
  ENV DEBIAN_FRONTEND=noninteractive
  ENV LANG=en_US.UTF-8
  ENV LANGUAGE=en_US:en
  ENV LC_ALL=en_US.UTF-8
  ENV VIRTUAL_ENV="/home/app/venv"
  ENV PATH="$VIRTUAL_ENV/bin:$PATH"

  #configure WSGI server and healthcheck
  ENV fprocess="$VIRTUAL_ENV/bin/python index.py" \
      cgi_headers="true" \
      mode="http" \
      upstream_url="http://127.0.0.1:5000"

  # Add non root user
  RUN addgroup --system app \
      && adduser app --system --ingroup app \
      && chown -R app:app /home/app

  USER app
  WORKDIR /home/app/

  COPY --from=test depends_on .
  COPY --from=build --chown=app:app venv $VIRTUAL_ENV
  COPY --chown=app:app index.py .
  COPY --chown=app:app function/ function/
  COPY --from=watchdog --chown=app:app /fwatchdog ./
  RUN chmod ug+rx fwatchdog

  HEALTHCHECK --interval=5s CMD [ -e /tmp/.lock ] || exit 1

  CMD ["/home/app/fwatchdog"]
