# Copyright (c) Alex Ellis 2017. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for full license information.


from flask import request
from waitress import serve
import structlog
import os

from function import app
from function.handler import handle

logger = structlog.stdlib.get_logger()
logger.info(
    "logging initialized", log_level=os.environ.get("IP_LOG_LEVEL", "info").upper()
)


# distutils.util.strtobool() can throw an exception
def is_true(val):
    return len(val) > 0 and val.lower() == "true" or val == "1"


@app.route("/", defaults={"path": ""}, methods=["POST", "GET"])
@app.route("/<path:path>", methods=["POST", "GET"])
def main_route(path):
    ret = handle(request)
    return ret


if __name__ == "__main__":
    serve(app, host="0.0.0.0", port=5000)
