[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/function_ip?style=for-the-badge)](https://gitlab.com/japtain_cack/function_ip/-/tags)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline-status/japtain_cack/function_ip?branch=master&style=for-the-badge)](https://gitlab.com/japtain_cack/function_ip/-/pipelines)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/function_ip?style=for-the-badge)](https://gitlab.com/japtain_cack/function_ip/-/issues)
[![Docker Stars](https://img.shields.io/docker/stars/nsnow/ip?style=for-the-badge)](https://hub.docker.com/r/nsnow/ip)
[![Docker Pulls](https://img.shields.io/docker/pulls/nsnow/ip?style=for-the-badge)](https://hub.docker.com/r/nsnow/ip)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)

# function_ip

OpenFaaS Function to return your external ip address.

* https://fn.mimir-tech.org/function/ip
* https://fn.mimir-tech.org/function/ip?headers=true
